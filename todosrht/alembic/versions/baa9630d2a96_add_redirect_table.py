"""Add redirect table

Revision ID: baa9630d2a96
Revises: 51a887000801
Create Date: 2024-11-27 13:33:41.141147
"""

from alembic import op

# revision identifiers, used by Alembic.
revision = 'baa9630d2a96'
down_revision = '51a887000801'

def upgrade():
    op.execute("""
    CREATE TABLE redirect (
        id serial PRIMARY KEY,
        created timestamp without time zone NOT NULL,
        name character varying(256) NOT NULL,
        owner_id integer NOT NULL REFERENCES "user"(id) ON DELETE CASCADE,
        new_tracker_id integer NOT NULL REFERENCES tracker(id) ON DELETE CASCADE
    );
    """)

def downgrade():
    op.execute("DROP TABLE redirect;")

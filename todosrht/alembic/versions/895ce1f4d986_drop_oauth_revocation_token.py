"""Drop oauth_revocation_token

Revision ID: 895ce1f4d986
Revises: 8f822cabf78b
Create Date: 2024-10-31 13:05:16.903841

"""

# revision identifiers, used by Alembic.
revision = '895ce1f4d986'
down_revision = '8f822cabf78b'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("""
    ALTER TABLE "user" DROP COLUMN oauth_revocation_token;
    """)


def downgrade():
    op.execute("""
    ALTER TABLE "user"
    ADD COLUMN oauth_revocation_token character varying(256);
    """)
